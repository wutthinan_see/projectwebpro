<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class controllerLink extends Controller
{
    public function store1(){
        $products=DB::table('product')->get();
    	return view('store',['products'=>$products]);
    }
    public function store2(){
        $productA=DB::table('productaccessorie')->get();
        return view('store2',['productA'=>$productA]);
    }
    public function store3(){
         $productM=DB::table('productmonitor')->get();
        return view('store3',['productM'=>$productM]);
    }

    public function home1(){
    	return view('index');
    }
    public function product1($id){
        $product = DB::table('product')->where('pid','=',$id)->get();
    	return view('product',['product'=>$product]);
    }
     public function product2($id){
        $productacc = DB::table('productaccessorie')->where('aid','=',$id)->get();
        return view('product2',['productacc'=>$productacc]);
    }
         public function product3($id){
        $productMo = DB::table('productmonitor')->where('mid','=',$id)->get();
        return view('product3',['productMo'=>$productMo]);
    }

     public function register1(){
    	return view('register');
    }
    public function login1(){
    	return view('login');
    }
   
    public function checklogin(Request $request){
        session_start();
        $data = $request->all();
        $result = array(
            'username' => $data['username'],
            'password' => $data['pass']
        );
        $custumer = DB::table('custumer')->where($result)->get();
        if(count($custumer)==1){
            $_SESSION['username']=$custumer[0]->username;
            if($custumer[0]->Status=='admin'){
                return view('Admin/HAdmin');
            }

            return redirect('index');
        }else{
            $message = "Login Fail";
          echo "<script type='text/javascript'>alert('$message');</script>";
           return view('login');
        }

        //return;
    }
    public function logout(){
        session_start();
        session_destroy();
        return redirect('index');
    }
  public function Admin(){
        return view('Admin/HAdmin');
    }
    public function viewmember(){
        $data = DB::table('custumer')->get();
        return view('Admin/viewmember',['data'=>$data]);
    }
    public function viewproduct(){
        $db1 = DB::table('product')->get();
        return view('Admin/adpro',['db1'=>$db1]);
    }
    public function adpro(){
        return view('Admin/viewpro');
    }
     public function insert_data(Request $request){

        $data = $request->all();
        //dd($data['address']);
        $result = array(
            'username' => $data['username'],
            'password' => $data['pass'],
            'email' => $data['email'],
            'address' => $data['address'],
        );
        DB::table('custumer')->insert($result);
        return view('index');
    }
    public function add(Request $request){

        $data = $request->all();
        //dd($data['address']);
        $result = array(
            'pid' => $data['pid'],
            'p_name' => $data['name'],
            'p_brand' => $data['brand'],
            'p_price' => $data['price'],
            'p_description' => $data['description'],
        );
        DB::table('product')->insert($result);
        
        return redirect('index');
    }
    
      public function homexxx(){
        return redirect('index');
    }
}

