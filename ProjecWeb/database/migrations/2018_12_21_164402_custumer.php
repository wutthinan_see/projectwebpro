<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Custumer extends Migration
{
    
    public function up()
    {
         Schema::create('custumer', function (Blueprint $table) {
            $table->increments('cid');
            $table->string('username');
             $table->string('password');
              $table->string('email');
               $table->string('address');
            $table->timestamp('created_at')->nullable();
        });
    }
 
    public function down()
    {
        //
    }
}
