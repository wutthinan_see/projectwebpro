<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Product extends Migration
{
    
        public function up()
    {
         Schema::create('product', function (Blueprint $table) {
            $table->increments('pid');
            $table->string('p_name');
            $table->string('p_price');
            $table->string('p_brand');
            $table->string('p_description');
            $table->string('p_img');
             
        });
    }

    
    public function down()
    {
        //
    }
}
