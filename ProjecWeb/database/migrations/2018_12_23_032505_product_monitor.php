<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductMonitor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
           public function up()
    {
         Schema::create('productMonitor', function (Blueprint $table) {
            $table->increments('mid');
            $table->string('m_name');
            $table->string('m_price');
            $table->string('m_brand');
            $table->string('m_description');
            $table->string('m_img');
             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
