<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/store','controllerLink@store1');
Route::get('/store2','controllerLink@store2');
Route::get('/store3','controllerLink@store3');



Route::get('/product/{id}','controllerLink@product1');
Route::get('/productA/{id}','controllerLink@product2');
Route::get('/productM/{id}','controllerLink@product3');

Route::get('/index','controllerLink@home1');
 
Route::get('/logins','controllerLink@login1');
Route::get('/register','controllerLink@register1');

Route::post('/logins','controllerLink@insert_data');

Route::post('/checklogin','controllerLink@checklogin');

Route::get('/logout','controllerLink@logout');

Route::get('/Admin','controllerLink@Admin');
Route::get('/Viewmember','controllerLink@viewmember');
Route::get('/Viewproduct','controllerLink@viewproduct');
Route::get('/Adminproduct','controllerLink@adpro');

Route::get('/loginxxx','controllerLink@homexxx');

// Route::get('/Adminproduct', function () {
//     return view('Admin/Adpro');
// });

Route::get('/AdminEdit', function () {
    return view('Admin/Adminedit');
});

// Route::get('/Viewproduct', function () {
//     return view('Admin/viewpro');
// });
Route::get('/login', function () {
    return view('Admin/login');
});
// Route::get('/HomeAdmin', function () {
//     return view('Admin/HAdmin');
// });

Route::post('/insertp','controllerLink@add');

